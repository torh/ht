#include <string.h>
#include <stdio.h>
#include "minunit.h"
#include "../src/ht.h"

int tests_run = 0;

static char * test_ht_create() {
  Hashtable *table = ht_create(200);
  mu_assert("error, bucket size not set properly", ht_get_bucket_size(table) == 200);
  return 0;
}

static char * test_ht_put_get() {
  Hashtable *table = ht_create(200);
  ht_put(table, "test", "foo");
  mu_assert("error, couldn't get from hash table", strcmp(ht_get(table, "test"), "foo") == 0);
  return 0;
}

static char * test_ht_remove() {
  Hashtable *table = ht_create(200);
  ht_put(table, "test", "foo");
  ht_remove(table, "test");
  mu_assert("error, couldn't remove from hash table", ht_get(table, "test") == NULL);
  return 0;
}

static char * all_tests() {
  mu_run_test(test_ht_create);
  mu_run_test(test_ht_put_get);
  mu_run_test(test_ht_remove);
  return 0;
}

int main(int argc, char **argv) {
  char *result = all_tests();
  if (result != 0) {
    printf("%s\n", result);
  }
  else {
    printf("ALL TESTS PASSED\n");
  }
  printf("Tests run: %d\n", tests_run);

  return result != 0;
}
