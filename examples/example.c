#include <stdio.h>
#include "../src/ht.h"

int main(char *argv[], int argc) {
  Hashtable *table = ht_create(20);

  ht_put(table, "test", "test");
  if (strcmp(ht_get(table, "test"), "test") == 0) {
    printf("Test 1 success\n");
  }

  ht_put(table, "xyz", "front 242");
  if (strcmp(ht_get(table, "xyz"), "front 242") == 0) {
    printf("Test 2 success\n");
  }

  ht_remove(table, "xyz");
  if (ht_get(table, "xyz") == NULL) {
    printf("Test 3 success\n");
  }

  ht_destroy(table);
  return 0;
}
