SOURCES=src/ht.c
TESTS=tests/minunit.h tests/ht_tests.c
EXAMPLE=examples/example.c
CC=gcc
BUILD_DIR=build

build_examples:
	@mkdir -p $(BUILD_DIR)
	@$(CC) -o $(BUILD_DIR)/example $(SOURCES) $(EXAMPLE)
	@echo "Executable 'example' available in build/ directory"

run_tests:
	@mkdir -p $(BUILD_DIR)
	@echo "Building tests executable"
	@$(CC) -o $(BUILD_DIR)/run_tests $(SOURCES) $(TESTS)
	@echo "Running tests..."
	@$(BUILD_DIR)/run_tests
	@rm $(BUILD_DIR)/run_tests

clean:
	rm -rf $(BUILD_DIR)/*
