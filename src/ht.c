#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include "ht.h"

struct _pair {
  unsigned int    hash;
  char            *key;
  void            *value;
  struct _pair    *next;
  struct _pair    *prev;
};

struct _bucket {
  struct _pair    *first_pair;
};

struct _hashtable {
  int             bucket_size;
  struct _bucket  **buckets;
};

/* djb2 hashing algorithm */
static unsigned long hash(char *str) {
  unsigned long hash = 5381;
  int c;

  while ((c = *str++))
      hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

  return hash;
}

Hashtable *ht_create(int bucket_size) {
  Hashtable *table = (Hashtable*) malloc(sizeof *table);
  Bucket **buckets = malloc(bucket_size * sizeof *buckets);
  int i = 0;

  table->bucket_size = bucket_size;
  table->buckets = buckets;

  for (; i < bucket_size; i++) {
    Bucket *bucket = malloc(sizeof *bucket);
    bucket->first_pair = NULL;
    table->buckets[i] = bucket;
  }

  return table;
}

int ht_get_bucket_size(Hashtable *table) {
  return table->bucket_size;
}

int ht_put(Hashtable *table, char *key, void *value) {
  if (table == NULL || key == NULL || value == NULL) {
    return -1;
  }

  unsigned int key_hash     = hash(key);
  unsigned int bucket_index = key_hash % table->bucket_size;

  Bucket *bucket   = table->buckets[bucket_index];
  Pair   *new_pair = malloc(sizeof *new_pair);

  new_pair->hash  = key_hash;
  new_pair->key   = key;
  new_pair->value = value;
  new_pair->prev  = NULL;
  new_pair->next  = NULL;

  if (bucket->first_pair != NULL) {
    bucket->first_pair->prev = new_pair;
    new_pair->next = bucket->first_pair;
  }
  bucket->first_pair = new_pair;

  return 1;
}

void *ht_get(Hashtable *table, char *key) {
  unsigned int key_hash = hash(key);
  unsigned int bucket_index = key_hash % table->bucket_size;

  Bucket *bucket = table->buckets[bucket_index];
  Pair   *pair   = bucket->first_pair;

  while (pair != NULL) {
    if (pair->hash == key_hash
          && pair->key != NULL
          && pair->value != NULL
          && strcmp(pair->key, key) == 0) {
      return pair->value;
    }
    pair = pair->next;
  }

  return NULL;
}

int ht_remove(Hashtable *table, char *key) {
  unsigned int key_hash     = hash(key);
  unsigned int bucket_index = key_hash % table->bucket_size;

  Bucket *bucket = table->buckets[bucket_index];
  Pair   *pair   = bucket->first_pair;

  while (pair != NULL) {
    if (pair->hash == key_hash
          && pair->key != NULL
          && pair->value != NULL
          && strcmp(pair->key, key) == 0) {
      if (pair->next != NULL) {
        ((Pair*)pair->next)->prev = pair->prev;
      }
      if (pair->prev != NULL) {
        ((Pair*)pair->prev)->next = pair->next;
      } else {
        /* First pair in bucket */
        bucket->first_pair = NULL;
      }
      free(pair);
      return 1;
    }
    pair = pair->next;
  }
  return 0;
}

int ht_destroy_bucket(Bucket *bucket) {
  Pair *pair = bucket->first_pair;
  while (pair != NULL) {
    Pair *next_pair = pair->next;
    free(pair);
    pair = next_pair;
  }
  free(bucket);
  return 1;
}

int ht_destroy(Hashtable *table) {
  int i = 0;
  for (; i < table->bucket_size; i++) {
    Bucket *bucket = table->buckets[i];
    free(bucket);
  }
  free(table);
  return 1;
}
