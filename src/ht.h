typedef struct _hashtable Hashtable;
typedef struct _bucket    Bucket;
typedef struct _pair      Pair;

Hashtable *ht_create(int bucket_size);
int       ht_get_bucket_size(Hashtable *table);
int       ht_put(Hashtable *table, char *key, void *value);
void      *ht_get(Hashtable *table, char *key);
int       ht_remove(Hashtable *table, char *key);
int       ht_destroy(Hashtable *table);